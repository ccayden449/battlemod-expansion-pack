rawset(_G, "redflag", 0)
rawset(_G, "blueflag", 0)

addHook("NetVars", function(n)
	redflag = n($)
	blueflag = n($)
end)

addHook("MobjThinker", function(mo)
	redflag = mo
end, MT_REDFLAG)

addHook("MobjThinker", function(mo)
	blueflag = mo
end, MT_BLUEFLAG)

addHook("ThinkFrame", do
	for p in players.iterate
		if p.valid and p.gotflag
			if p.gotflag == GF_BLUEFLAG
				blueflag = p.mo
			elseif p.gotflag == GF_REDFLAG
				redflag = p.mo
			end
		end
	end
	if redflag and not redflag.valid
		redflag = nil
	end
	if blueflag and not blueflag.valid
		blueflag = nil
	end
end)